# Node server 

## Server start 
```
npm run start
```

## Eslint 
```
npm run lint
```

## Mongo Compass
```
sudo service mongod start
```
