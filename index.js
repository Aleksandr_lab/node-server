const express = require('express');
const mongoose = require('mongoose');
require('./app/models/product');
const config = require('./config');

const app = express();
config.express(app);
config.routes(app);

const { port, mongoUri, mongoOptions } = config.app;

mongoose.connect(mongoUri, mongoOptions)
  .then(() => app.listen(
    port,
    () => console.log(`Listen port ${port}...`),
  ))
  .catch((err) => console.error(`Error connectiong to mongo: ${mongoUri}`, err));

/* const path = require('path');
 index.get('/', (req, res) => res.sendFile(path.join(`${__dirname}/index.html`))); */
