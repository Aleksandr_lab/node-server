module.exports = {
  port: process.env.PORT || 3000,
  mongoUri: 'mongodb://localhost:27017/online-store',
  mongoOptions: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  },
};
